/**
 * Automatically generated file. DO NOT MODIFY
 */
package fr.o80.miandroid;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "fr.o80.miandroid.debug";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 100010;
  public static final String VERSION_NAME = "1.0.10";
}
