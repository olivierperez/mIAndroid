// Generated code from Butter Knife. Do not modify!
package fr.o80.miandroid;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import fr.o80.miandroid.presentation.MainActivity;

import java.lang.IllegalStateException;
import java.lang.Override;

public class MainActivity_ViewBinding<T extends MainActivity> implements Unbinder {
  protected T target;

  private View view2131624057;

  @UiThread
  public MainActivity_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.main_ask, "field 'ask' and method 'onImeAction'");
    target.ask = Utils.castView(view, R.id.main_ask, "field 'ask'", EditText.class);
    view2131624057 = view;
    ((TextView) view).setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView p0, int p1, KeyEvent p2) {
        return target.onImeAction(p1);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.ask = null;

    ((TextView) view2131624057).setOnEditorActionListener(null);
    view2131624057 = null;

    this.target = null;
  }
}
