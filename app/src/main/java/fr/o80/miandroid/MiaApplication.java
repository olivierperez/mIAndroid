package fr.o80.miandroid;

import android.app.Application;

import fr.o80.miandroid.di.DaggerMiaComponent;
import fr.o80.miandroid.di.MiaComponent;

/**
 * @author Olivier Perez
 */
public class MiaApplication extends Application {

    private MiaComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerMiaComponent.builder().build();
    }

    public MiaComponent component() {
        return component;
    }
}
