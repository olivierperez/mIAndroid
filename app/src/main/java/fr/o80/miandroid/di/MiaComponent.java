package fr.o80.miandroid.di;

import dagger.Component;
import fr.o80.miandroid.presentation.MainActivity;

/**
 * @author Olivier Perez
 */
@Component
public interface MiaComponent {
    void inject(MainActivity activity);
}
