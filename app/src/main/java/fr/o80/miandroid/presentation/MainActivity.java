package fr.o80.miandroid.presentation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnEditorAction;
import fr.o80.miandroid.MiaApplication;
import fr.o80.miandroid.R;

public class MainActivity extends AppCompatActivity implements MainView {

    @BindView(R.id.main_ask)
    protected EditText ask;

    @Inject
    protected MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        ((MiaApplication)getApplication()).component().inject(this);

        presenter.attach(this);
    }

    @OnEditorAction(R.id.main_ask)
    protected boolean onImeAction(int actionId, EditText editText) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            Toast.makeText(this, "Done", Toast.LENGTH_SHORT).show();

            presenter.ask(editText.getText().toString());
            return false;
        }
        return true;
    }

    @Override
    public void clear() {
        ask.setText("");
    }
}
