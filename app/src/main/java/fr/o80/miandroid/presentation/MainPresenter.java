package fr.o80.miandroid.presentation;

import javax.inject.Inject;

/**
 * @author Olivier Perez
 */
public class MainPresenter {

    private MainView view;

    @Inject
    public MainPresenter() {
    }

    public void ask(String text) {
        view.clear();
    }

    public void attach(MainView view) {
        this.view = view;
    }

}
